import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import{AngularFireModule} from 'angularfire2';

export const firebaseConfig = {


apiKey: "AIzaSyCVHHSVGfPbgmxXf9yxGcAu3XfSq5km4Dk",
    
authDomain: "mylogin-2969c.firebaseapp.com",
    
databaseURL: "https://mylogin-2969c.firebaseio.com",
    
storageBucket: "mylogin-2969c.appspot.com",
   
 messagingSenderId: "167126377776"
  
 

}

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: []
})
export class AppModule {}
